import { Asset } from "../../store/modules/assets";

const assets: Asset[] = [];

export const createAsset = async (asset: Asset) => {
  assets.push(asset);
  return asset;
};

export const fetchAssets = async () => {
  return assets;
};

export const getAsset = async (assetName: string) => {
  return assets.find((asset) => asset.name === assetName);
};
