import { createStore } from "vuex";
import { assets, AssetsState } from "./modules/assets";

export interface State {
  assets: AssetsState;
}

const store = createStore<State>({
  modules: {
    assets,
  },
});

export default store;
