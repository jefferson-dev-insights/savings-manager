import { Module } from "vuex";
import { createAsset } from "../../services/assetService";
import { State } from "../index";

// interface Deposit {
//   date: Date;
//   ammount: Number;
// }

// interface ValueUpdate extends Deposit {}

type Deposit = number;
type ValueUpdate = number;

export interface Asset {
  id: string;
  name: string;
  deposits: Deposit[];
  valueUpdates: ValueUpdate[];
}

export interface AssetGoals {
  name: string;
  current: number;
  goal: number;
  invest: number;
}

export interface AssetsState {
  assets: Asset[];
}

interface CreateAsset {
  name: string;
}

export const assets: Module<AssetsState, State> = {
  state: () => {
    return {
      assets: [],
    };
  },
  actions: {
    async createAsset({ commit }, { name }: CreateAsset) {
      const newAsset = {
        id: String(Math.floor(Math.random() * 100)),
        name,
        deposits: [1, 1, 1, 1],
        valueUpdates: [1, 1, 1, 1].map((value) => value * 1.2),
      };
      const asset = await createAsset(newAsset);
      commit("addAsset", asset);
    },
  },
  mutations: {
    addAsset(state, asset: Asset) {
      state.assets.push(asset);
    },
  },
};
