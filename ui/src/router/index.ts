import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import AssetDetails from "../views/AssetDetails.vue";
import Goals from "../views/Goals.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/asset/:assetId",
    name: "AssetDetails",
    component: AssetDetails,
  },
  {
    path: "/goals/",
    name: "Goals",
    component: Goals,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
